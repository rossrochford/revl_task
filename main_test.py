from val.regions_countries import Germany, UK, France, Texas, Alaska, Colorado

if __name__ == '__main__':
    # for brevity, we'll use assertions for tests

    assert Germany.calculate_vat('power_drill', 20) == 0.15*20
    assert Germany.calculate_vat('bread', 1.5) == germany_bread(1.5)
    assert Germany.calculate_vat('wine', 8.25) == 0.20*8.25

    assert UK.calculate_vat('power_drill', 15) == uk_base_vat(15)
    assert UK.calculate_vat('power_drill', 45) == uk_base_vat(45)
    assert UK.calculate_vat('power_drill', 215) == uk_base_vat(215)
    assert UK.calculate_vat('wine', 8.5) == 0.1*8.5

    assert France.calculate_vat('power_drill', 15) == 0.125 * 15
    assert France.calculate_vat('wine', 50) == 5.0
    assert France.calculate_vat('eggs', 3) == (3-0.5) * 0.125
    assert France.calculate_vat('beer', 4) == 4 * 0.175
    assert France.calculate_vat('ale', 3) == 3 * 0.175

    assert Texas.calculate_vat('power_drill', 30) == 0.125*30
    assert Texas.calculate_vat('beer', 30) == (0.1 * (30-1)) + (0.075*30)
    # etc...


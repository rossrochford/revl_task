from types import FunctionType

from vat.override_functions import texas_beer
from vat.vat_logic import Percentage, FlatVatValue, AdditionalVatPercentage, VatMaxCap, TaxFreeThresh
from vat.product_categories import PRODUCT_CATEGORIES


class Region(object):

    BASE_VAT = None
    PRODUCT_OVERRIDES = None


class EU(Region):

    BASE_VAT = Percentage(12.5)
    PRODUCT_OVERRIDES = {
        'bread': FlatVatValue(0.05)
    }


class USA(Region):

    BASE_VAT = Percentage(10)
    PRODUCT_OVERRIDES = {
        'dairy': Percentage(5),
        'alcohol': AdditionalVatPercentage(7.5)
    }


class Country(object):

    BASE_VAT = None
    REGION = None
    PRODUCT_OVERRIDES = None

    @classmethod
    def _get_overrides(cls, product_name):
        """
        Gets the 'overrides' for a particular product bought in a
        particular country. An override is some kind of rule that
        describes how the tax should be calculated. It finds override
        rules in order of specificity:
            1) specific product in a country
            2) product category in a country
            3) specific product in a region
            4) product category in a region
        However, if a function is found, it takes precendence and only
        that is returned. The function is responsible for the total
        calculation logic for a particular product/category/country/region.
        """
        overrides = []
        category = PRODUCT_CATEGORIES.get(product_name)

        if product_name in cls.PRODUCT_OVERRIDES:
            ov = cls.PRODUCT_OVERRIDES[product_name]
            if isinstance(ov, FunctionType):
                return [ov]
            overrides.append(ov)

        if category and category in cls.PRODUCT_OVERRIDES:
            ov = cls.PRODUCT_OVERRIDES[category]
            if isinstance(ov, FunctionType):
                return [ov]
            overrides.append(ov)

        if product_name in cls.REGION.PRODUCT_OVERRIDES:
            overrides.append(cls.REGION.PRODUCT_OVERRIDES[product_name])

        if category and category in cls.REGION.PRODUCT_OVERRIDES:
            overrides.append(cls.REGION.PRODUCT_OVERRIDES[category])

        return overrides

    @classmethod
    def calculate_vat(cls, product_name, price):
        """
        Calculates vat amount based on the product/category/country/region.

        It applies overrides in a specific order so it is limited in its
        ability to express some vat rules. For all other cases you should
        write a specific calculation function, calculate_vat() will spot the
        function and use it as an override.
        """
        overrides = cls._get_overrides(product_name)
        base_val_func = cls.BASE_VAT or cls.REGION.BASE_VAT

        ov_types = [type(ov) for ov in overrides]

        if FunctionType in ov_types:
            # er, not sure about this, what about when a function is placed on a region or product-category
            # and it steamrolls all other parameters on the product- or country-level?
            func = overrides[ov_types.index(function_type)]
            # NOTE: functions are responsible for calculating the complete
            # amount, including any tax-free amount or vat max-cap.
            return func(price, base_val_func)

        if FlatVatValue in ov_types:
            flat_val = overrides[ov_types.index(FlatVatValue)]
            return flat_val.val

        if TaxFreeThresh in ov_types:
            # assumes we should always take the first (most specific) tax-free
            # rule and ignore any others. Another option would be to use the maximum
            # or throw an exception when more than one exists.
            obj = overrides[ov_types.index(TaxFreeThresh)]
            price = price - obj.val

        val = None
        for i, ov in enumerate(overrides):
            ov_type = ov_types[i]
            if ov_type is Percentage:
                # a base VAT override for a product in a specific country or region
                val = ov(price)
                break

        if val is None:
            val = base_val_func(price)

        if AdditionalVatPercentage in ov_types:
            ov = overrides[ov_types.index(VatMaxCap)]
            val = val + (price*ov.val)

        if VatMaxCap in ov_types:
            ov = overrides[ov_types.index(VatMaxCap)]
            return min(ov.val, val)

        return val


class Germany(Country):

    BASE_VAT = Percentage(15)
    REGION = EU  
    PRODUCT_OVERRIDES = {
        'bread': TaxFreeThresh(1),
        'wine': Percentage(20)
    }


class UK(Country):
    BASE_VAT = uk_base_vat,
    REGION = EU,
    PRODUCT_OVERRIDES = {
        'wine': Percentage(10)
    }


class France(Country):
    REGION = EU
    PRODUCT_OVERRIDES = {
        'wine': VatMaxCap(5.0),
        'eggs': TaxFreeThresh(0.50),
        'beer': Percentage(17.5),
        'ale': Percentage(17.5)
    }


# for simplicity we're calling states in the USA a 'country'
class Texas(Country):
    REGION = USA
    BASE_VAT = Percentage(12.5)
    PRODUCT_OVERRIDES = {
        'beer': texas_beer,
        'bread': FlatVatValue(0.05)
    }


class Alaska(Country):
    REGION = USA
    PRODUCT_OVERRIDES = {
        'milk': VatMaxCap(0.50),
        # beer? description in task is incomplete
    }


class Colorado(Country):
    BASE_VAT = Percentage(11)
    PRODUCT_OVERRIDES = {
        'milk': VatMaxCap(2*0.1),  # only first $2 is taxable, this is equivalent to a VatMaxCap
        'bread': Percentage(5)
    }



# assumes a simple product taxonomy of 0 or 1 categories per product
# and only one level of categories
PRODUCT_CATEGORIES = {
    'milk': 'dairy',
    'eggs': 'dairy',
    'beer': 'alcohol',
    'ale': 'alcohol',
    'wine': 'alcohol',
}
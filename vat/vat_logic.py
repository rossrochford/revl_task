
class FlatVatValue(object):
    def __init__(self, val):
        self.val = val

    def __call__(self, price):
        return self.val


class VatMaxCap(object):

    def __init__(self, val):
        self.val = val

    def __call__(self, price):
        return min(self.val, price)


class TaxFreeThresh(object):

    def __init__(self, val):
        self.val = val

    # we won't call() this


class AdditionalVatPercentage(object):

    def __init__(self, val):
        self.val = val/100

    # def __call__(self, price):
    #     return price + (self.val*price)


class Percentage(object):

    def __init__(self, val):
        self.val = val/100

    def __call__(self, price):
        return self.val*price